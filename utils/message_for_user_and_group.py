from aiogram.types import Message

from utils.text import TextMessages

async def messages_for_user_and_group(message: Message, text: str, language: str) -> tuple[str]:
    """Get messages for the user and developer group."""
    message_for_group = await TextMessages().message_for_developer_group(
        message.from_user.id,
        message.from_user.username,
        message.from_user.full_name,
        text
    )
    message_for_user = await TextMessages().thankful_message(language)
    return message_for_group, message_for_user
