from datetime import datetime

class TextMessages:
    """
    This object represent the text that the bot send to the user and developer group.
    """
    async def about_language(self) -> str:
        """Use this method to get a message about choosing language."""
        return "Выберите язык / Choose language"

    async def about_chosen_language(self, language: str) -> str:
        """Use this method to get a message about chosen language."""
        ru = "Выбран язык: 🇷🇺 Русский"
        en = "Selected language: 🇬🇧 English"
        message_language = {"ru": ru, "en": en}
        return message_language[language]
    
    async def language_buttons_text(self) -> list[str]:
        """Use this method to get a text of the language buttons."""
        ru = "🇷🇺 Русский"
        en = "🇬🇧 English"
        return ru, en
    
    async def report_buttons_text(self, language: str) -> list[str]:
        """Use this method to get a text of the report buttons."""
        ru = [
            "Баг 🐞",
            "Предложение 💡"
        ]
        en = [
            "Bug 🐞",
            "Suggestion 💡"
        ]
        message_language = {"ru": ru, "en": en}
        return message_language[language]
    
    async def welcome_message(self, language: str) -> str:
        """Use this method to get a welcome message."""
        ru = "Привет!\n\n" \
             "Спасибо, что обратились к нашему боту! Мы готовы помочь вам в решении любых " \
             "проблем и вопросов. Если вы заметили какие-либо баги или неисправности, пожалуйста, " \
             "предоставьте максимально подробное описание проблемы. \n\n"\
             "Если у вас есть предложения или идеи по улучшению, мы также готовы их принять. \n\n" \
             "Пожалуйста, выберите один из следующих вариантов, чтобы продолжить:"
        en = "Hello!\n\n" \
             "Thank you for contacting our bot! We are ready to help you with any problems and questions. " \
             "If you have noticed any bugs or malfunctions, please provide as detailed a description of " \
             "the problem as possible.\n\n"\
             "If you have any suggestions or ideas for improvement, we are also ready to accept them.\n\n" \
             "Please select one of the following options to continue:"
        message_language = {"ru": ru, "en": en}
        return message_language[language]
    
    async def message_describing_bug(self, language: str) -> str:
        """Use this method to get a message about how the user should describing the bug."""
        ru = "Отлично! Расскажите о возникшей проблеме, и мы немедленно приступим к ее исправлению. Для того чтобы " \
             "более эффективно разобраться в ситуации, предоставьте, пожалуйста, следующую информацию: \n" \
             "1. Укажите, на какой странице был обнаружен баг \n\n" \
             "2. Если баг связан с определенными элементами или функциями, опишите их \n" \
             "3. Расскажите, какое поведение или результат вы ожидали от системы \n" \
             "4. Опишите, что происходит на самом деле или какой результат вы получили \n" \
             "5. Пришлите скриншоты или видео с обнаруженной проблемой \n" \
             "Ваша детальная информация поможет нам точно определить проблему и решить ее как можно скорее. Мы " \
             "благодарим вас за активное участие и готовность помочь! Ожидаем вашего сообщения."
        en = "Great! Tell us about the problem you're experiencing, and we'll get to work fixing it right away. " \
             "To help us better understand the situation, please provide the following information: \n" \
             "1. Specify on which page the bug was found \n\n" \
             "2. If the bug is related to specific elements or functions, describe them \n" \
             "3. Tell us what behavior or result you expected from the system \n" \
             "4. Describe what actually happens or what result you got \n" \
             "5. Send screenshots or a video of the problem you found \n" \
             "Your detailed information will help us pinpoint the problem and fix it as soon as possible. We " \
             "thank you for your active participation and willingness to help! We look forward to your message."
        message_language = {"ru": ru, "en": en}
        return message_language[language]
    
    async def message_describing_suggestion(self, language: str) -> str:
        """Use this method to get message about how the user migth describing their suggestion."""
        ru = "Мы всегда стремимся предоставить наилучший опыт использования нашей системы, и ваше мнение " \
             "очень важно! Если у вас есть какие-либо пожелания, то мы будем рады их услышать. Спасибо, " \
             "что делаете нас лучше! Ждем ваших идей и предложений."
        en = "We always strive to provide the best experience using our system, and your opinion is very " \
             "important! If you have any wishes, we would be glad to hear them. Thank you for making us " \
             "better! We are waiting for your ideas and suggestions."
        message_language = {"ru": ru, "en": en}
        return message_language[language]
    
    async def commands_description(self, language: str) -> list[str]:
        """Use this method to get a description of commands."""
        ru = [
            "Старт",
            "Есть баг",
            "Есть предложение",
            "Изменить язык"
        ]
        en = [
            "Start",
            "There's bug",
            "I have a suggestion",
            "Change language"
        ]
        description_language = {"ru": ru, "en": en}
        return description_language[language]
    
    async def thankful_message(self, language: str) -> str:
        """Use this method to get thankful message to the user."""
        ru = "Спасибо за помощь в улучшении нашей системы. Ваше предложение будет рассмотрено! \n\n" \
             "Если у вас остались еще какие-нибудь замечания и предложения по работе нашего сервиса, " \
             "выберите нужное и напишите нам снова."
        en = "Thank you for helping us improve our system. Your suggestion will be considered! \n\n" \
             "If you have any other comments or suggestions about our service, please select the one " \
             "you need and write to us again."
        description_language = {"ru": ru, "en": en}
        return description_language[language]
    
    async def message_for_developer_group(self, user_id: int, user_name: str, full_name: str, text: str) -> str:
        """Use this method to get message in developer group."""
        return f"{'Пользователь прислал только медиафайл(ы).' if not text else text} \n\n" \
               f"Сообщение от пользователя {full_name} - @{user_name} \n" \
               f"id засранца: {user_id} \n" \
               f"{datetime.now().strftime('%d.%m.%Y')}"
