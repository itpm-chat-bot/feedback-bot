from asyncio import sleep

from aiogram.types import Message, InputMediaPhoto, InputMediaVideo, InputMediaDocument
from aiogram.fsm.context import FSMContext

from handlers.start import start
from keyboards.inline import get_start_keyboard
from states.report_state import Report
from utils.message_for_user_and_group import messages_for_user_and_group
from utils.text import TextMessages
from data.config import GROUP_ID, THREAD_ID
from loader import bot

MEDIA_GROUPS = dict()

async def send_messages(
        message_type: str,
        message: Message,
        state: FSMContext,
        message_for_group: str,
        message_for_user: str,
        language: str,
        album: list[InputMediaPhoto | InputMediaVideo | InputMediaDocument] = None,
        file: tuple[str] = None) -> None:
    """Send a user bug report or suggestion to the developer group."""
    if album:
        if message_type == "bug":
            await bot.send_media_group(chat_id=GROUP_ID, media=album)
            await bot.send_message(chat_id=GROUP_ID, text=message_for_group)
        else:
            await bot.send_media_group(chat_id=GROUP_ID, message_thread_id=THREAD_ID, media=album)
            await bot.send_message(chat_id=GROUP_ID, message_thread_id=THREAD_ID, text=message_for_group)
    elif file:
        if file[0] == "photo":
            if message_type == "bug":
                await bot.send_photo(chat_id=GROUP_ID, photo=file[-1], caption=message_for_group)
            else:
                await bot.send_photo(
                    chat_id=GROUP_ID, message_thread_id=THREAD_ID, photo=file[-1], caption=message_for_group
                )
        elif file[0] == "video":
            if message_type == "bug":
                await bot.send_video(chat_id=GROUP_ID, video=file[-1], caption=message_for_group)
            else:
                await bot.send_video(
                    chat_id=GROUP_ID, message_thread_id=THREAD_ID, video=file[-1], caption=message_for_group
                )
        else:
            if message_type == "bug":
                await bot.send_document(chat_id=GROUP_ID, document=file[-1], caption=message_for_group)
            else:
                await bot.send_document(
                    chat_id=GROUP_ID, message_thread_id=THREAD_ID, document=file[-1], caption=message_for_group
                )
    elif message_for_group:
        if message_type == "bug":
            await bot.send_message(chat_id=GROUP_ID, text=message_for_group)
        else:    
            await bot.send_message(chat_id=GROUP_ID, message_thread_id=THREAD_ID, text=message_for_group)
    bug_btn, suggestion_btn = await TextMessages().report_buttons_text(language)
    markup = await get_start_keyboard(bug_btn, suggestion_btn)
    await message.answer(text=message_for_user, reply_markup=markup)
    await state.set_state(Report.choose_type_report)


async def processing_message(message: Message, state: FSMContext, language: str, message_type: str) -> None:
    """Process a user message containing any files or text."""
    # Processing only text
    if message.text:
        if message.text.lower() == "/start":
            await start(message, state)
        else:
            msg_for_group, msg_for_user = await messages_for_user_and_group(message, message.text, language)
            await send_messages(message_type, message, state, msg_for_group, msg_for_user, language)
    # Processing only media group
    elif message.media_group_id:
        if message.photo:
            if message.media_group_id in MEDIA_GROUPS:
                MEDIA_GROUPS[message.media_group_id].append(f"{message.photo[-1].file_id}photo")
                return
            MEDIA_GROUPS[message.media_group_id] = [f"{message.photo[-1].file_id}photo"]
        if message.video:
            if message.media_group_id in MEDIA_GROUPS:
                MEDIA_GROUPS[message.media_group_id].append(f"{message.video.file_id}video")
                return
            MEDIA_GROUPS[message.media_group_id] = [f"{message.video.file_id}video"]
        if message.document:
            if message.media_group_id in MEDIA_GROUPS:
                MEDIA_GROUPS[message.media_group_id].append(message.document.file_id)
                return
            MEDIA_GROUPS[message.media_group_id] = [message.document.file_id]
        await sleep(1)
        album = [
            InputMediaPhoto(media=file_id[:-5]) if file_id[-5:] == "photo" else
            InputMediaVideo(media=file_id[:-5]) if file_id[-5:] == "video" else
            InputMediaDocument(media=file_id)
            for file_id in MEDIA_GROUPS[message.media_group_id]
        ]
        MEDIA_GROUPS.pop(message.media_group_id, None)
        msg_for_group, msg_for_user = await messages_for_user_and_group(message, message.caption, language)
        await send_messages(message_type, message, state, msg_for_group, msg_for_user, language, album)
    # Processing only single media file
    elif any([message.photo, message.video, message.document]):
        if message.photo:
            file_id = message.photo[-1].file_id
            file_type = "photo"
        elif message.video:
            file_id = message.video.file_id
            file_type = "video"
        else:
            file_id = message.document.file_id
            file_type = "document"
        file_info = (file_type, file_id)
        msg_for_group, msg_for_user = await messages_for_user_and_group(message, message.caption, language)
        await send_messages(message_type, message, state, msg_for_group, msg_for_user, language, album=None, file=file_info)
        