from aiogram.types.bot_command import BotCommand
from aiogram.types.bot_command_scope_default import BotCommandScopeDefault

from utils.text import TextMessages
from loader import bot

async def set_default_commands(descriptions: list[str] = None) -> None:
    """Set default commands in bot menu."""
    if not descriptions:
        descriptions = await TextMessages().commands_description("ru")
    command_names = ["start", "bug", "suggestion", "language"]
    commands = [
        BotCommand(command=command_names[i], description=descriptions[i])
        for i in range(len(command_names))
    ]
    await bot.set_my_commands(commands=commands, scope=BotCommandScopeDefault())
