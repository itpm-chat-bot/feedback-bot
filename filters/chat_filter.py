from aiogram.enums import ChatType
from aiogram.filters import BaseFilter
from aiogram.types import Message

from data.config import GROUP_ID

class IsPrivate(BaseFilter):
    async def check(self, message: Message) -> bool:
        return message.chat.type == ChatType.PRIVATE


class IsOurGroup(BaseFilter):
    async def check(self, message: Message) -> bool:
        return message.chat.id == GROUP_ID
