from environs import Env

env = Env()
env.read_env()

BOT_TOKEN = env.str("BOT_TOKEN")
BOT_TOKEN_DEV = env.str("BOT_TOKEN_DEV", BOT_TOKEN)
ADMINS = env.list("ADMINS")

GROUP_ID = env.str("GROUP_ID")    # ID группы продакшена
DEV_GROUP_ID = env.str("DEV_GROUP_ID", GROUP_ID)    # ID группы разработчиков

THREAD_ID = env.str("THREAD_ID")    # THREAD ID топика "Предложения" из группы продакшена
DEV_THREAD_ID = env.str("DEV_THREAD_ID", THREAD_ID)    # THREAD ID топика "Предложения" из группы разработчиков

REDIS_HOST=env.str("REDIS_HOST")
REDIS_PORT=env.str("REDIS_PORT")
REDIS_DB=env.str("REDIS_DB")

SENTRY_SDK = env.str("SENTRY_SDK")
