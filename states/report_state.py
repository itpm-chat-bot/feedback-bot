from aiogram.fsm.state import StatesGroup, State

class Report(StatesGroup):
    choose_type_report = State()
    send_bug_report = State()
    send_suggestion = State()
