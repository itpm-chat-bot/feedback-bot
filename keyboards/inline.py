from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
from aiogram.utils.keyboard import InlineKeyboardBuilder

from utils.text import TextMessages

async def get_start_keyboard(text_bug_btn: str, text_suggestion_btn: str) -> InlineKeyboardMarkup:
    builder = InlineKeyboardBuilder()
    builder.row(
        InlineKeyboardButton(text=text_bug_btn, callback_data="bug"),
        InlineKeyboardButton(text=text_suggestion_btn, callback_data="suggestion")
    )
    kb_markup = builder.as_markup()
    return kb_markup


async def get_language_keyboard(action: str) -> InlineKeyboardMarkup:
    """Creates two buttons for choosing language."""
    builder = InlineKeyboardBuilder()
    text_ru_btn, text_en_btn = await TextMessages().language_buttons_text()
    ru_btn = InlineKeyboardButton(text=text_ru_btn, callback_data=f"language_{action}_ru")
    en_btn = InlineKeyboardButton(text=text_en_btn, callback_data=f"language_{action}_en")
    builder.add(ru_btn, en_btn)
    builder.adjust(2)
    return builder.as_markup()
