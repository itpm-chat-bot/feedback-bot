from aiogram import F, Router
from aiogram.filters import Command
from aiogram.fsm.context import FSMContext
from aiogram.types import Message, CallbackQuery

from handlers.language import get_language
from states.report_state import Report
from utils.processing_messages import processing_message
from utils.text import TextMessages

bug_router = Router()

@bug_router.message(Report.send_bug_report, F.photo | F.video | F.document | F.text)
async def processing_message_about_bug(message: Message, state: FSMContext) -> None:
    """Begin the process of processing a user message about found bug."""
    language = await get_language(user_key=f"language:{message.from_user.id}")
    await processing_message(message, state, language, message_type="bug")


@bug_router.callback_query(Report.choose_type_report, F.data=="bug")
async def describe_bug(callback: CallbackQuery, state: FSMContext) -> None:
    """Send a prompt to the user to describe the found bug."""
    await state.set_state(Report.send_bug_report)
    language = await get_language(user_key=f"language:{callback.from_user.id}")
    text = await TextMessages().message_describing_bug(language)
    await callback.message.answer(text=text)


@bug_router.message(Command("bug"))
async def bug(message: Message, state: FSMContext) -> None:
    """Send a prompt to the user to describe the found bug."""
    await state.set_state(Report.send_bug_report)
    language = await get_language(user_key=f"language:{message.from_user.id}")
    text = await TextMessages().message_describing_bug(language)
    await message.answer(text=text)
