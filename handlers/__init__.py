"""Import all routers and add them to routers_list."""
from .bug import bug_router
from .reaction import reaction_router
from .suggestion import suggestion_router
from .start import start_router
from .language import language_router

routers_list = [
    start_router,
    bug_router,
    suggestion_router,
    language_router,
    reaction_router,
]

__all__ = [
    "routers_list",
]