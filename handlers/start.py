from aiogram import F, Router
from aiogram.filters import Command
from aiogram.types import Message

from keyboards.inline import get_language_keyboard
from middlewares.throttling import ThrottlingMiddleware
from utils.text import TextMessages
from utils.misc.redis import redis

start_router = Router()
start_router.message.middleware(ThrottlingMiddleware(redis))

@start_router.message(Command("start"))
async def start(message: Message) -> None:
    """Send message about choose language and keyboard with actions."""
    language_message = await TextMessages().about_language()
    markup = await get_language_keyboard("noupdate")
    await message.answer(text=language_message, reply_markup=markup)
