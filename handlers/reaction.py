from emoji import emojize

from aiogram import F, Router
from aiogram.types import Message
from aiogram.types.message_reaction_updated import MessageReactionUpdated
from aiogram.types.message_reaction_count_updated import MessageReactionCountUpdated
from aiogram.types.reaction_type_emoji import ReactionTypeEmoji
from aiogram.types.reaction_type_custom_emoji import ReactionTypeCustomEmoji

from loader import bot

reaction_router = Router()
USERS = {}
BAN_EMOJI = ["🤡", "😐", "❤‍🔥"]


@reaction_router.message_reaction_count()
async def reaction(msg_reaction: MessageReactionCountUpdated):
    """Получает апдейт об изменившемся количестве реакций на сообщении."""
    print("reaction count")
    print(type(msg_reaction), msg_reaction.model_dump_json())


@reaction_router.message_reaction()
async def reaction(msg_reaction: MessageReactionUpdated):
    """Обрабатывает реакции, которые пользователи поставили на любые сообщения в чате."""
    print("reaction", msg_reaction.model_dump_json())
    emoji = ReactionTypeEmoji(type="emoji", emoji="🔥")
    try:
        if msg_reaction.new_reaction[0].emoji == "🤡":
            await bot.send_message(msg_reaction.chat.id, f"Сам ты клоун, @{msg_reaction.user.username}!")
            await bot.set_message_reaction(msg_reaction.chat.id, msg_reaction.message_id, [emoji])
    except IndexError:
        pass


@reaction_router.message(F.text)
async def set_message_reaction(message: Message):
    """Бот ставит реакции на сообщения пользователей."""
    emoji_1 = ReactionTypeEmoji(type="emoji", emoji="🔥")
    emoji_2 = ReactionTypeEmoji(type="emoji", emoji="🖕")
    emoji_3 = ReactionTypeEmoji(type="emoji", emoji="🤡")
    if message.text == "hello":
        await bot.set_message_reaction(message.chat.id, message.message_id, [emoji_1])
        await bot.send_message(message.chat.id, f"Привет @{message.from_user.username}!")
    elif message.text == "bye":
        await bot.set_message_reaction(message.chat.id, message.message_id, [emoji_2])
        await bot.send_message(message.chat.id, f"Пока @{message.from_user.username}!")
    else:
        await bot.set_message_reaction(message.chat.id, message.message_id, [emoji_3])
        await bot.send_message(message.chat.id, emojize("Напиши что-нибудь нормальное, ебалай :thumbs_up:"))
