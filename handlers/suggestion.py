from aiogram import F, Router
from aiogram.filters import Command
from aiogram.fsm.context import FSMContext
from aiogram.types import Message, CallbackQuery

from handlers.language import get_language
from states.report_state import Report
from utils.processing_messages import processing_message 
from utils.text import TextMessages

suggestion_router = Router()

@suggestion_router.message(Report.send_suggestion, F.photo | F.video | F.document | F.text)
async def processing_message_about_suggestion(message: Message, state: FSMContext) -> None:
    """Begin the process of processing a user message about suggestion."""
    language = await get_language(user_key=f"language:{message.from_user.id}")
    await processing_message(message, state, language, message_type="suggestion")


@suggestion_router.callback_query(Report.choose_type_report, F.data=="suggestion")
async def describe_suggestion(callback: CallbackQuery, state: FSMContext) -> None:
    """Send a prompt to the user to describe the suggestion."""
    await state.set_state(Report.send_suggestion)
    language = await get_language(user_key=f"language:{callback.from_user.id}")
    text = await TextMessages().message_describing_suggestion(language)
    await callback.message.answer(text=text)


@suggestion_router.message(Command("suggestion"))
async def suggestion(message: Message, state: FSMContext) -> None:
    """Send a prompt to the user to describe the suggestion."""
    await state.set_state(Report.send_suggestion)
    language = await get_language(user_key=f"language:{message.from_user.id}")
    text = await TextMessages().message_describing_suggestion(language)
    await message.answer(text=text)
