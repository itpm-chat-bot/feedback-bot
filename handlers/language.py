from aiogram import F, Router
from aiogram.filters import Command
from aiogram.fsm.context import FSMContext
from aiogram.types import CallbackQuery, Message

from keyboards.inline import get_start_keyboard, get_language_keyboard
from middlewares.throttling import ThrottlingMiddleware
from states.report_state import Report
from utils.set_bot_commands import set_default_commands
from utils.text import TextMessages
from utils.misc.redis import redis

language_router = Router()
language_router.message.middleware(ThrottlingMiddleware(redis))

async def get_language(user_key: str) -> str:
    """Returns language wich choosed user."""
    language = redis.hget(name=user_key, key="language")
    return language


@language_router.message(Command("language"))
async def language(message: Message) -> None:
    """Send message about choose language and keyboard with actions."""
    language_message = await TextMessages().about_language()
    markup = await get_language_keyboard("update")
    await message.answer(text=language_message, reply_markup=markup)


@language_router.callback_query(F.data.startswith("language"))
async def choose_language(callback_query: CallbackQuery, state: FSMContext) -> None:
    """Set up language for user to the bot chat and update language for user to the adminpanel."""
    user_key = f"language:{callback_query.from_user.id}"
    language = callback_query.data.split("_")[-1]
    redis.hset(name=user_key, key="language", value=language)

    choosed_language_message = await TextMessages().about_chosen_language(language)
    await callback_query.answer(text=choosed_language_message)
    
    # Reset default commands in bot menu for chat bot
    await set_default_commands(
        descriptions=await TextMessages().commands_description(language)
    )

    # Send welcome message or ignore
    if callback_query.data.split("_")[1] == "update":
        return
    else:
        choosed_language = await get_language(user_key)
        text_message = await TextMessages().welcome_message(choosed_language)
        bug_btn, suggestion_btn = await TextMessages().report_buttons_text(choosed_language)
        keyboard = await get_start_keyboard(bug_btn, suggestion_btn)
        await callback_query.message.answer(text=text_message, reply_markup=keyboard)
        await state.set_state(Report.choose_type_report)