# Feedback Bot


### Our bots

https://t.me/ITPM_dev_feedback_bot - bot for developers to work with. Here you can test your new features before they go into production.  
https://t.me/feedback_itpmbot - production version of the bot. This bot has a stable version of the product and is used by our regular users.


### Install for development

1. Clone this repo `git clone https://gitlab.com/itpm-chat-bot/feedback-bot.git` and go to it's root directory.
2. Install dependencies: `pip install -r requirements.txt`.
3. Set up Git pre-commit hooks: `pre-commit install`.
4. Rename `.env.template` file in the root folder to `.env` and fill it in.
5. Run bot: `python bot.py`.


### Run in Docker

1. Clone this repo `git clone https://gitlab.com/itpm-chat-bot/feedback-bot.git` and go to it's root folder.
2. Rename `.env.template` file in the root folder to `.env` and fill it in.
3. Run `docker compose up -d`.