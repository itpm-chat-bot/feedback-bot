FROM python:3.10-buster

WORKDIR /usr/src/app/tg_feedback_bot

COPY requirements.txt /usr/src/app/tg_feedback_bot
RUN pip install -r /usr/src/app/tg_feedback_bot/requirements.txt
COPY . /usr/src/app/tg_feedback_bot
